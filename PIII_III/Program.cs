﻿﻿using System;
using System.Globalization;
using System.Text;

namespace PIII_III
{
    class Program
    {
        static void Main(string[] args)
        {
            #region stringi
            
            StringBuilder sb = new StringBuilder();
            string text = "@";
            while (text != String.Empty)
            {
                text = Console.ReadLine();
                sb.Append(text+';');   
            }
            
            text = sb.ToString();
            string[] grades = text.Split(";", StringSplitOptions.RemoveEmptyEntries);
            float sum = 0;
            foreach (var item in grades)
            {
                sum += float.Parse(item, CultureInfo.InvariantCulture.NumberFormat);
            }
            
            #endregion
            
            FirstTask ft = new FirstTask();
            Console.WriteLine("The average grade is {0:#.##}", ft.CountAverage(sum, grades.Length));
        }
    }
}